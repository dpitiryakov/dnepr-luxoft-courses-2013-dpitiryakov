package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.concurrent.atomic.AtomicInteger;


public class HitCount extends HttpServlet{

    private  static AtomicInteger hintCounter=new AtomicInteger(0);

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        hintCounter.incrementAndGet();
        response.setStatus(200);
        response.setContentType("application/json; charset=utf-8");
        String tmp="\"hitCount\": "+hintCounter.toString();
        response.setContentLength(tmp.length());
        writer.print(tmp);

    }
}
