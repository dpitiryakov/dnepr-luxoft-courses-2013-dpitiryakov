package com.luxoft.courses.javaWeb;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class Dummy extends HttpServlet {

    public static final String ERROR_ILLEGAL_PARAMETERS = "{\"error\": \"Illegal parameters\"}";
    private static Map<String, Integer> nameAndAgeDB = new ConcurrentHashMap<>();

    protected void doPut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter printWriter = response.getWriter();
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        response.setContentType("application/json; charset=utf-8");
        if (name == null || age == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            printWriter.println(ERROR_ILLEGAL_PARAMETERS);
        } else if (nameAndAgeDB.containsKey(name)) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            printWriter.println("{\"error\": \"Name " + name + " already exists\"}");
        } else {
            nameAndAgeDB.put(name, Integer.valueOf(age));
            response.setStatus(HttpServletResponse.SC_CREATED);
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter printWriter = response.getWriter();
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        response.setContentType("application/json; charset=utf-8");
        System.out.println(name);
        System.out.println(age);
        if (name == null || age == null) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            printWriter.println(ERROR_ILLEGAL_PARAMETERS);
        } else if (!nameAndAgeDB.containsKey(name)) {
            response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            printWriter.println("{\"error\": \"Name " + name + " does not exists\"}");
        } else {
            nameAndAgeDB.put(name, Integer.valueOf(age));
            response.setStatus(HttpServletResponse.SC_ACCEPTED);
        }
    }

}