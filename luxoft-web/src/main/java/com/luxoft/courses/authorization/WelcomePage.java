package com.luxoft.courses.authorization;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;


public class WelcomePage extends HttpServlet {


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (request.getSession(false)==null){
            response.sendRedirect("/index.html");
            return;
        }
        PrintWriter writer = response.getWriter();
        String login= (String) request.getSession().getAttribute("login");

        writer.print("<html>\n" +
                "\t<head>\n" +
                "\t\t<title> A Web Page</title>\n" +
                "\t</head>\n" +
                "\t<body> \n" +
                "\t\t<div align=\"right\">\n" +
                "\t\t\t<a href=\"/logout\"> Log out</a>\n" +
                "\t\t</div>\n" +
                "\t\t<div align=\"center\">\n" +
                "\t\t\t<br>\n" +
                "\t\t\t\tHello " + login + "!\n" +
                "\t\t\t</br>\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</body>\n" +
                "\n" +
                "</html>");
    }

}
