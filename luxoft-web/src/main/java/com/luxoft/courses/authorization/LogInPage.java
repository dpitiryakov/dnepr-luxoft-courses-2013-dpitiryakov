package com.luxoft.courses.authorization;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class LogInPage extends HttpServlet {
    public static final String WRONG_LOGIN_OR_PASSWORD = "Wrong login or password";

    private boolean errorFlag=false; // false - no mistake, true - there is a mistake
    private static List<String> names = new ArrayList<>();
    private static List<String> passwords = new ArrayList<>();

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String contextParamJSON=getServletContext().getInitParameter("users");
        feelNamesAndPasswords(contextParamJSON, names, passwords);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String tmpError="";
        if (errorFlag){
            tmpError=WRONG_LOGIN_OR_PASSWORD;
        }
        writer.print("<html>\n" +
                "\t<head>\n" +
                "\t\t<title> A Web Page</title>\n" +
                "\t</head>\n" +
                "\n" +
                "\t<body>\n" +
                "\t\t<div id=\"container\" align=\"center\"> \n" +
                "\t\t\t<form action=\"index.html\" method=\"post\">\n" +
                "\t\t\t\t<p>\n" +
                "\t\t\t\t\t<br>\n" +
                "\t\t\t\t\t\t Login:\n" +
                "\t\t\t\t\t\t<input name=\"name\" id=\"nameField\" type=\"text\"  />\n" +
                "\t\t\t\t\t\t" + tmpError + "\n" +
                "\t\t\t\t\t</br>\n" +
                "\t\t\t\t\t<br> \n" +
                "\t\t\t\t\t\tPassword:\n" +
                "\t\t\t\t\t\t<input name=\"password\" id=\"passwordField\" type=\"password\"  />\n" +
                "\t\t\t\t\t</br>\n" +
                "\t\t\t\t\t<input name=\"loginBtn\" type=\"submit\" value=\"Sign In\">\n" +
                "\t\t\t\t</p>\n" +
                "\t\t\t</form>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</body>\n" +
                "\n" +
                "</html>");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,IOException {
        String nameTyped=request.getParameter("name");
        String passwordTyped=request.getParameter("password");


        if (names.contains(nameTyped) && passwords.contains(passwordTyped)){
            errorFlag=false;
            request.getSession().setAttribute("login", nameTyped);
            response.sendRedirect("/user");
        } else {
            errorFlag=true;
            response.sendRedirect("/index.html");
        }
    }

    private void feelNamesAndPasswords(String contextParamJSON, List<String> names, List<String> passwords) {
        Pattern p = Pattern.compile("\"(.*?)\": \"(.*?)\"") ;
        Matcher m = p.matcher(contextParamJSON);
        while(m.find()){
            names.add(m.group(1));
            passwords.add(m.group(2));
        }
    }
}
