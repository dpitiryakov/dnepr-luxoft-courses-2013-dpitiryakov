package com.luxoft.dnepr.courses.regular.unit5.dao;

import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Entity;
import com.luxoft.dnepr.courses.regular.unit5.storage.EntityStorage;

import java.util.Collections;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 11.05.13
 * Time: 15:36
 * To change this template use File | Settings | File Templates.
 */
public abstract class AbstractDao<E extends Entity> implements IDao<E> {


    public E save(E entity) throws UserAlreadyExist {
        if (entity.getId() == null) {
            if (EntityStorage.getInstance().getEntities().size() == 0)
                entity.setId(new Long(1));
            else
                entity.setId(Collections.max(EntityStorage.getInstance().getEntities().keySet()) + 1);
        }

        if (EntityStorage.getInstance().getEntities().containsKey(entity.getId())) {
            throw new UserAlreadyExist("Such user has already exist. Id: "+entity.getId());
        }
        EntityStorage.getInstance().getEntities().put(entity.getId(), entity);

        return entity;

    }

    @Override
    public E update(E entity) throws UserNotFound {
        if (EntityStorage.getEntities().get(entity.getId()) == null) {
            throw new UserNotFound("User with such id="+entity.getId()+" wasn't found.");
        }
        EntityStorage.getInstance().getEntities().put(entity.getId(), entity);
        return entity;
    }

    @Override
    public E get(long id) {
        return (E)EntityStorage.getInstance().getEntities().get(id);
    }

    @Override
    public boolean delete(long id) {
        return EntityStorage.getInstance().getEntities().remove(id) != null;
    }



}
