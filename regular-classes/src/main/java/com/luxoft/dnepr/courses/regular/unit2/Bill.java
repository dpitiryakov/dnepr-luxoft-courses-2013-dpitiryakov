package com.luxoft.dnepr.courses.regular.unit2;

import java.util.*;

/**
 * Represents a bill.
 * Combines equal {@link Product}s and provides information about total price.
 */
public class Bill {
    private List<CompositeProduct> CompositeProducts = new ArrayList<CompositeProduct>();
    /**
     * Appends new instance of product into the bill.
     * Groups all equal products using {@link CompositeProduct}
     * @param product new product
     */
    public void append(Product product) {
        for (CompositeProduct compositeProduct : CompositeProducts) {
            if (product.equals(compositeProduct.getFirst())) {
                compositeProduct.add(product);
                return;
            }
        }
        CompositeProduct tmpComp = new CompositeProduct();
        tmpComp.add(product);
        CompositeProducts.add(tmpComp);

    }

    /**
     * Calculates total cost of all the products in the bill including discounts.
     * @return
     */
    public double summarize() {
        if (CompositeProducts.size() == 0) return 0;
        double sum = 0;
        for (CompositeProduct compositeProduct : CompositeProducts) {
            sum += compositeProduct.getPrice();
        }
        return sum;

    }

    /**
     * Returns ordered list of products, all equal products are represented by single element in this list.
     * Elements should be sorted by their price in descending order.
     * See {@link CompositeProduct}
     * @return
     */
    public List<Product> getProducts() {
        Collections.sort(CompositeProducts, new CompositeProductComparable());
        List<Product> products = new ArrayList<Product>();
        for (CompositeProduct compositeProduct : CompositeProducts) {
            Product product = compositeProduct;
            products.add(product);
        }
        return products;

    }


    public class CompositeProductComparable implements Comparator<CompositeProduct> {
        @Override
        public int compare(CompositeProduct p1, CompositeProduct p2) {
            return (p1.getPrice() > p2.getPrice() ? -1 : (p1.getPrice() == p2.getPrice() ? 0 : 1));
        }
    }


    @Override
    public String toString() {
        List<String> productInfos = new ArrayList<String>();
        for (Product product : getProducts()) {
            productInfos.add(product.toString());
        }
        return productInfos.toString() + "\nTotal cost: " + summarize() ;
    }

}
