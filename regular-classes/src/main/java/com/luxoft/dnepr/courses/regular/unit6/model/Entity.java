package com.luxoft.dnepr.courses.regular.unit6.model;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 10.05.13
 * Time: 14:38
 * To change this template use File | Settings | File Templates.
 */
public class Entity {
    private Long id;
    public Long getId() { return id; }
    public void setId(Long id) { this.id = id; }

    public Entity(Long id) {
        this.id = id;
    }
}
