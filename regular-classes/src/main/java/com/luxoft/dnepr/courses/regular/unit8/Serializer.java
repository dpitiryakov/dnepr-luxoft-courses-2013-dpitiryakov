package com.luxoft.dnepr.courses.regular.unit8;

import java.io.*;


public class Serializer {
    public static void serialize(File file, FamilyTree entity) {
        try {
            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            entity.writeExternal(oos);
            oos.flush();
            oos.close();
            fos.close();
        } catch (FileNotFoundException e) {
            System.out.print("File not found Exception.");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    public static FamilyTree deserialize(File file) {
        FamilyTree entity=new FamilyTree();
        try {
            FileInputStream fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            entity = (FamilyTree) ois.readObject();
            ois.close();
            fis.close();
        } catch (ClassNotFoundException e) {
            System.out.print("Class not found Exception.");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return entity;

    }
}