package com.luxoft.dnepr.courses.regular.unit2;

public class Bread extends AbstractProduct implements Product {


    private double weight;

    public Bread(String code, String name, double price, double weight) {
        super(code,name,price);
        this.weight=weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Bread)) return false;
        if (!super.equals(o)) return false;

        Bread bread = (Bread) o;

        if (Double.compare(bread.weight, weight) != 0) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(weight);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }


}
