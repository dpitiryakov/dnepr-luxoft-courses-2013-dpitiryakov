package com.luxoft.dnepr.courses.regular.unit5.model;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 10.05.13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class Redis extends Entity {
    private int weight;
    public int getWeight() { return weight; }
    public void setWeight(int weight) { this.weight = weight; }

    public Redis(Long id, int weight) {
        super(id);
        this.weight = weight;
    }
}