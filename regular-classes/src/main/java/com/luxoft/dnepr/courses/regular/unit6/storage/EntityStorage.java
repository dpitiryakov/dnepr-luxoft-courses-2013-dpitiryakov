package com.luxoft.dnepr.courses.regular.unit6.storage;

import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 10.05.13
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();
    private static EntityStorage instance;
    private EntityStorage() {}
    ArrayList<String> star=new ArrayList<>();

    public static synchronized Long getNextId(){
        if (entities.size()==0)
            return new Long(1);
        else
            return Collections.max(entities.keySet()) + 1;
    }

    public static EntityStorage getInstance() {
        if (instance == null) {
            instance = new EntityStorage();
        }
        return instance;
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }
}
