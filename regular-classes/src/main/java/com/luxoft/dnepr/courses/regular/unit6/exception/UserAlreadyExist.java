package com.luxoft.dnepr.courses.regular.unit6.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 11.05.13
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
public class UserAlreadyExist extends RuntimeException {
    public UserAlreadyExist(String message) {
        super(message);
    }
}
