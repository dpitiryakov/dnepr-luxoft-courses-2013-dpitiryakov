package com.luxoft.dnepr.courses.compiler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

public final class ShuntingYardAlgorithm {
    private ShuntingYardAlgorithm() {
    }

    // Supported operators
    private static final Map<String, Integer> OPERATORS = new HashMap<String, Integer>();

    static {
        OPERATORS.put("+", 0);
        OPERATORS.put("-", 0);
        OPERATORS.put("*", 5);
        OPERATORS.put("/", 5);
    }

    /**
     * Test if a certain is an operator .
     *
     * @param token The token to be tested .
     * @return True if token is an operator . Otherwise False .
     */
    public static boolean isOperator(String token) {
        return OPERATORS.containsKey(token);
    }

    /**
     * Test if a certain is a number .
     *
     * @param token The token to be tested .
     * @return True if token is a number . Otherwise False .
     */
    public static boolean isNumeric(String token) {
        return token.matches("\\d+(\\.\\d+)?");
    }

    /**
     * Compare precedence of two operators.
     *
     * @param token1 The first operator .
     * @param token2 The second operator .
     * @return A negative number if token1 has a smaller precedence than token2,
     *         0 if the two tokens are equal, a positive number
     *         otherwise.
     */
    public static final int cmpPrecedence(String token1, String token2) {
        if (!isOperator(token1) || !isOperator(token2)) {
            throw new IllegalArgumentException("Invalid tokens: " + token1
                    + " " + token2);
        }
        return OPERATORS.get(token1) - OPERATORS.get(token2);
    }

    /**
     * Change tokens order from infix to Reverse Polish Notation
     * using shunting-yard algorithm.
     *
     * @param inputTokens
     * @return RPN form of inputTokens
     */
    public static String[] infixToRPN(String[] inputTokens) {
        ArrayList<String> out = new ArrayList<String>();
        Stack<String> stack = new Stack<String>();
        for (String token : inputTokens) {
            if (isOperator(token)) {
                while (!stack.empty() && isOperator(stack.peek())) {
                    if (cmpPrecedence(token, stack.peek()) <= 0) {
                        out.add(stack.pop());
                        continue;
                    }
                    break;
                }
                stack.push(token);
            } else if (token.equals("(")) {
                stack.push(token);
            } else if (token.equals(")")) {
                while (!stack.empty() && !stack.peek().equals("(")) {
                    out.add(stack.pop());
                }
                stack.pop();
            } else {
                out.add(token);
            }
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        String[] output = new String[out.size()];
        return out.toArray(output);
    }
}