package com.luxoft.dnepr.courses.regular.unit3.exceptions;


public class WalletIsBlockedException extends Exception {
    private Long walletId;

    public WalletIsBlockedException(String message, Long walletId) {
        super(message);
        this.walletId = walletId;
    }

    public Long getWalletId() {
        return walletId;
    }
}
