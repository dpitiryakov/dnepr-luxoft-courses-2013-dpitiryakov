package com.luxoft.dnepr.courses.regular.unit6.model;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 10.05.13
 * Time: 14:39
 * To change this template use File | Settings | File Templates.
 */
public class Employee extends Entity {
    private int salary;
    public int getSalary() { return salary; }
    public void setSalary(int salary) { this.salary = salary; }

    public Employee(Long id, int salary) {
        super(id);
        this.salary = salary;
    }
}