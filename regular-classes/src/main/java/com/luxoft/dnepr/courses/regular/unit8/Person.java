package com.luxoft.dnepr.courses.regular.unit8;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.util.Date;



public class Person implements Externalizable {


    private static final String BIRTHDATE = "\"birthDate\":";
    private static final String END = "}";
    private static final String ETHNICITY = "\"ethnicity\":";
    private static final String FATHER = "\"father\":";
    private static final String GENDER = "\"gender\":";
    private static final String MOTHER = "\"mother\":";
    private static final String NAME = "\"name\":";
    private static final String SEPARATOR = "\",";
    private static final String ROOT = "{\"root\":{";


    private String name;
    private Gender gender;
    private String ethnicity;
    private Date birthDate;
    private Person father;
    private Person mother;

    @Override
    public void readExternal(ObjectInput in) throws IOException, ClassNotFoundException {

    }
    
    @Override
    public void writeExternal(ObjectOutput out) throws IOException {
        StringBuffer sb = new StringBuffer();
        sb.append(ROOT);
        toJSON(sb);
        out.writeUTF(sb.toString());
        sb.append(END);
    }

    private void toJSON(StringBuffer sb) {
        if (name != null) sb.append(NAME).append(name).append(SEPARATOR);
        if (gender != null) sb.append(GENDER).append(gender.toString()).append(SEPARATOR);
        if (ethnicity != null) sb.append(ETHNICITY).append(ethnicity).append(SEPARATOR);
        if (birthDate != null) sb.append(BIRTHDATE).append(birthDate.getTime()).append(SEPARATOR);
        if (father != null) {
            sb.append("\"").append(FATHER).append("\":{");
            father.toJSON(sb);
        }
        if (mother != null) {
            sb.append("\"").append(MOTHER).append("\":{");
            mother.toJSON(sb);
        }
        sb.append(END);

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public String getEthnicity() {
        return ethnicity;
    }

    public void setEthnicity(String ethnicity) {
        this.ethnicity = ethnicity;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Person getFather() {
        return father;
    }

    public void setFather(Person father) {
        this.father = father;
    }

    public Person getMother() {
        return mother;
    }

    public void setMother(Person mother) {
        this.mother = mother;
    }
}
