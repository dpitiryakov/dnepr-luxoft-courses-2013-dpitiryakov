package com.luxoft.dnepr.courses.compiler;

import java.io.ByteArrayOutputStream;
import java.util.*;

public class Compiler {
	
	public static final boolean DEBUG = true;


    public static void main(String[] args) {
		byte[] byteCode = compile(getInputString(args));
		VirtualMachine vm = VirtualMachineEmulator.create(byteCode, System.out);
		vm.run();
	}

	static byte[] compile(String input) {
        if (input==null) throw new CompilationException("Input string has come as null");
		ByteArrayOutputStream result = new ByteArrayOutputStream();
        String regex = "(?<=[-+*/()])|(?=[-+*/()])";
        input=input.replaceAll("\\s","");
        String[] temp=input.split(regex);
        String[] output = ShuntingYardAlgorithm.infixToRPN(temp);
        writeCommands(result, output);
		return result.toByteArray();
	}

    /**
     * Writes commands from output to result
     * @param result
     * @param output
     */
    private static void writeCommands(ByteArrayOutputStream result, String[] output) {
        for (String token : output) {
            if (ShuntingYardAlgorithm.isOperator(token)){
                if (token.equals("+")){
                    addCommand(result, VirtualMachine.ADD);
                }
                if (token.equals("-")){
                    addCommand(result,VirtualMachine.SWAP);
                    addCommand(result,VirtualMachine.SUB);
                }
                if (token.equals("*")){
                    addCommand(result,VirtualMachine.MUL);
                }
                if (token.equals("/")){
                    addCommand(result,VirtualMachine.SWAP);
                    addCommand(result,VirtualMachine.DIV);
                }
            }
            if (ShuntingYardAlgorithm.isNumeric(token)) {
                addCommand(result,VirtualMachine.PUSH,Double.valueOf(token));
            }
        }
        addCommand(result,VirtualMachine.PRINT);
    }


    /**
	 * Adds specific command to the byte stream.
	 * @param result
	 * @param command
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command) {
		result.write(command);
	}
	
	/**
	 * Adds specific command with double parameter to the byte stream.
	 * @param result
	 * @param command
	 * @param value
	 */
	public static void addCommand(ByteArrayOutputStream result, byte command, double value) {
		result.write(command);
		writeDouble(result, value);
	}
	
	private static void writeDouble(ByteArrayOutputStream result, double val) {
		long bits = Double.doubleToLongBits(val);
		
		result.write((byte)(bits >>> 56));
		result.write((byte)(bits >>> 48));
		result.write((byte)(bits >>> 40));
		result.write((byte)(bits >>> 32));
		result.write((byte)(bits >>> 24));
		result.write((byte)(bits >>> 16));
		result.write((byte)(bits >>>  8));
		result.write((byte)(bits >>>  0));
	}
	
	private static String getInputString(String[] args) {
		if (args.length > 0) {
			return join(Arrays.asList(args));
		}
		
		Scanner scanner = new Scanner(System.in);
		List<String> data = new ArrayList<String>();
		data.add(scanner.next());
		return join(data);
	}

	private static String join(List<String> list) {
		StringBuilder result = new StringBuilder();
		for (String element : list) {
			result.append(element);
		}
		return result.toString();
	}

}
