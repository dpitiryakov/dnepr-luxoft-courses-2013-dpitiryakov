package com.luxoft.dnepr.courses.regular.unit6.dao;

import com.luxoft.dnepr.courses.regular.unit6.exception.EntityNotFoundException;
import com.luxoft.dnepr.courses.regular.unit6.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit6.model.Entity;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 10.05.13
 * Time: 14:44
 * To change this template use File | Settings | File Templates.
 */
public interface IDao<E extends Entity> {

    E save(E e) throws UserAlreadyExist;
    E update(E e) throws EntityNotFoundException;
    E get(long id);
    boolean delete(long id);
}