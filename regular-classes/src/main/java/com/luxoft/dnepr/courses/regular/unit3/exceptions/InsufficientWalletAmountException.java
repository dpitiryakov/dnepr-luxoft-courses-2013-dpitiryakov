package com.luxoft.dnepr.courses.regular.unit3.exceptions;


import java.math.BigDecimal;

public class InsufficientWalletAmountException extends Exception {
    private Long walletId;
    private BigDecimal amountToWithdraw;
    private BigDecimal amountInWallet;

    public InsufficientWalletAmountException(String message, Long walletId, BigDecimal amountToWithdraw, BigDecimal amountInWallet) {
        super(message);
        this.walletId = walletId;
        this.amountToWithdraw = amountToWithdraw;
        this.amountInWallet = amountInWallet;
    }

    public Long getWalletId() {
        return walletId;
    }

    public BigDecimal getAmountToWithdraw() {
        return amountToWithdraw;
    }

    public BigDecimal getAmountInWallet() {
        return amountInWallet;
    }
}
