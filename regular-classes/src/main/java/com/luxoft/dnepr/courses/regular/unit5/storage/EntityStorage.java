package com.luxoft.dnepr.courses.regular.unit5.storage;

import com.luxoft.dnepr.courses.regular.unit5.model.Entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 10.05.13
 * Time: 17:03
 * To change this template use File | Settings | File Templates.
 */
public class EntityStorage {
    private final static Map<Long, Entity> entities = new HashMap();
    private static EntityStorage instance;
    private EntityStorage() {}

    public static EntityStorage getInstance() {
        if (instance == null) {
            instance = new EntityStorage();
        }
        return instance;
    }

    public static Map<Long, Entity> getEntities() {
        return entities;
    }
}
