package com.luxoft.dnepr.courses.regular.unit2;

import java.util.Date;

public class Book extends AbstractProduct implements Product {


    private Date publicationDate;

    protected Book(String code, String name, double price, Date publicationDate) {
        super(code, name, price);
        this.publicationDate= (Date) publicationDate.clone(); //
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        Book copy=(Book)super.clone();
        copy.publicationDate=(Date)publicationDate.clone();
        return copy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;

        Book book = (Book) o;

        if (publicationDate != null ? !publicationDate.equals(book.publicationDate) : book.publicationDate != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (publicationDate != null ? publicationDate.hashCode() : 0);
        return result;
    }

    public Date getPublicationDate() {
        return publicationDate;
    }

    public void setPublicationDate(Date publicationDate) {
        this.publicationDate = publicationDate;
    }


}
