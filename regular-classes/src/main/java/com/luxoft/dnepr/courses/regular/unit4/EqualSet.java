package com.luxoft.dnepr.courses.regular.unit4;

import java.util.*;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 27.04.13
 * Time: 20:25
 * To change this template use File | Settings | File Templates.
 */
public class EqualSet<E> implements Set<E> {
    private List<E> data;
    private int size;

    public EqualSet() {
        data=new ArrayList<>();
    }

    public EqualSet(Collection<? extends E> collection) {
        data=new ArrayList<>();
        addAll(collection);
        size=data.size();
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean contains(Object o) {
        for (E e : data){
            if (e==null){
                if (o==null) return true;
            } else if (e.equals(o)) return true;
        }
        return false;
    }

    @Override
    public Iterator<E> iterator() {
        return data.iterator();
    }

    @Override
    public Object[] toArray() {
        return data.toArray();
    }

    @Override
    public boolean add(E el) {
        for (E e : data) {
            if (e == null) {
                if (el == null) return false;
            } else if (e.equals(el)) return false;
        }
        data.add(el);
        size = data.size();
        return true;
    }

    @Override
    public boolean remove(Object o) {
        if (data.remove(o)) {
            size=data.size();
            return true;
        }
        return false;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        for (Object e : c)
            if (!contains(e))
                return false;
        size=data.size();
        return true;
    }

    @Override
    public boolean addAll(Collection<? extends E> c) {
        boolean modified = false;
        for (E e : c)
            if (add(e))
                modified = true;
        size=data.size();
        return modified;
    }

    @Override
    public boolean retainAll(Collection<?> c) {
        boolean modified = false;
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            if (!c.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        size=data.size();
        return modified;
    }

    @Override
    public boolean removeAll(Collection<?> c) {
        boolean modified = false;
        Iterator<?> it = iterator();
        while (it.hasNext()) {
            if (c.contains(it.next())) {
                it.remove();
                modified = true;
            }
        }
        size=data.size();
        return modified;
    }

    @Override
    public void clear() {
        Iterator<E> it = iterator();
        while (it.hasNext()) {
            it.next();
            it.remove();
        }
        size=data.size();
    }

    @Override
    public Object[] toArray(Object[] a) {
        return data.toArray(a);
    }
}
