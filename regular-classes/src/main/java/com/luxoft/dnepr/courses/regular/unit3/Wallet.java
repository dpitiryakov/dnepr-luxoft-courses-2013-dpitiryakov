package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 23.04.13
 * Time: 13:08
 * To change this template use File | Settings | File Templates.
 */
public class Wallet implements WalletInterface {
    private Long id;
    private BigDecimal amount;
    private WalletStatus status;
    private BigDecimal maxAmount;
    private Set<String> mySet=new HashSet<>();

    public Wallet() {
    }

    public Wallet(Long id, BigDecimal amount, WalletStatus status, BigDecimal maxAmount) {
        this.id = id;
        this.amount = amount;
        this.status = status;
        this.maxAmount = maxAmount;
    }

    @Override
    public Long getId() {
        return id;
    }
    @Override
    public void setId(Long id) {
        this.id = id;
    }
    @Override
    public BigDecimal getAmount() {
        return amount;
    }
    @Override
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
    @Override
    public WalletStatus getStatus() {
        return status;
    }
    @Override
    public void setStatus(WalletStatus status) {
        this.status = status;
    }
    @Override
    public BigDecimal getMaxAmount() {
        return maxAmount;
    }
    @Override
    public void setMaxAmount(BigDecimal maxAmount) {
        this.maxAmount = maxAmount;
    }

    @Override
    public void checkWithdrawal(BigDecimal amountToWithdraw)
            throws WalletIsBlockedException, InsufficientWalletAmountException {
        checkWalletStatus();
        if (amountToWithdraw.compareTo(amount)==1)
            throw new InsufficientWalletAmountException("The amount to withdraw bigger " +
                    "then amount in wallet.",id, amountToWithdraw, amount);
    }

    private void checkWalletStatus() throws WalletIsBlockedException {
        if (status== WalletStatus.BLOCKED) throw new WalletIsBlockedException("User wallet is blocked!",id);
    }

    @Override
    public void withdraw(BigDecimal amountToWithdraw) {
        amount=amount.subtract(amountToWithdraw);
    }

    @Override
    public void checkTransfer(BigDecimal amountToTransfer) throws WalletIsBlockedException, LimitExceededException {
        checkWalletStatus();
        if (amountToTransfer.add(amount).compareTo(maxAmount)==1)
            throw new LimitExceededException("The sum of the amount to transfer and wallet amount is bigger then maxAmount.",id,amountToTransfer,amount);



    }

    @Override
    public void transfer(BigDecimal amountToTransfer) {
       amount=amount.add(amountToTransfer);
    }

    @Override
    public String getFormatAmount() {
        BigDecimal newBD = amount.setScale(2);
        return newBD.toPlainString();
    }
}
