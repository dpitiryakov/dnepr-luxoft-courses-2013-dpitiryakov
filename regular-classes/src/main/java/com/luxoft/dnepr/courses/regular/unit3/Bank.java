package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.*;

import java.math.BigDecimal;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 23.04.13
 * Time: 22:20
 * To change this template use File | Settings | File Templates.
 */
public class Bank implements BankInterface {
    private  Map<Long,UserInterface> users;

    public Bank() {
    }

    public Bank(String expectedJavaVersion){
        if(!System.getProperty("java.version").equals(expectedJavaVersion))
            throw new IllegalJavaVersionError(System.getProperty("java.version"),expectedJavaVersion,"Wrong java version!");
    }

    @Override
    public Map<Long, UserInterface> getUsers() {
        return users;
    }

    @Override
    public void setUsers(Map<Long, UserInterface> users) {
        this.users = users;
    }

    /**
     * Makes money transaction
     * @param fromUserId
     * @param toUserId
     * @param amount
     * @throws NoUserFoundException
     * @throws TransactionException
     */
    @Override
    public void makeMoneyTransaction(Long fromUserId, Long toUserId, BigDecimal amount) throws NoUserFoundException, TransactionException {
        checkUserIdForNull(fromUserId);
        checkUserIdForNull(toUserId);

        checkWithdrawal(fromUserId, amount);
        checkTransfer(toUserId, amount);

        users.get(fromUserId).getWallet().withdraw(amount);
        users.get(toUserId).getWallet().transfer(amount);
    }

    private void checkTransfer(Long toUserId, BigDecimal amount) throws TransactionException {
        try {
            users.get(toUserId).getWallet().checkTransfer(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '"+users.get(toUserId).getName() +"' wallet is blocked");      //!!!!
        } catch (LimitExceededException e) {
            throw  new TransactionException("User '"+users.get(toUserId).getName()+
                    "' wallet limit exceeded ("+formatBigDecimal(e.getAmountInWallet())+
                    " + "+formatBigDecimal(e.getAmountToTransfer())+
                    " > "+formatBigDecimal(users.get(toUserId).getWallet().getMaxAmount())+")");
        }
    }

    private void checkWithdrawal(Long fromUserId, BigDecimal amount) throws TransactionException {
        try {
            users.get(fromUserId).getWallet().checkWithdrawal(amount);
        } catch (WalletIsBlockedException e) {
            throw new TransactionException("User '"+users.get(fromUserId).getName() +"' wallet is blocked");      //!!!!
        } catch (InsufficientWalletAmountException e) {
            throw new TransactionException("User '"+users.get(fromUserId) +"' has insufficient funds ("+
                    formatBigDecimal(e.getAmountInWallet()) +" < " +
                    formatBigDecimal(e.getAmountToWithdraw())+")");
        }
    }

    private void checkUserIdForNull(Long fromUserId) throws NoUserFoundException {
        if (users.get(fromUserId)==null)
            throw new NoUserFoundException("User with id "+fromUserId.toString()+" not found",fromUserId);
    }

    private boolean userWalletIsBlocked(Long toUserId) {
        return users.get(toUserId).getWallet().getStatus()== WalletStatus.BLOCKED;
    }

    private String formatBigDecimal(BigDecimal bd){
        BigDecimal newBD = bd.setScale(2,BigDecimal.ROUND_HALF_UP);
        return newBD.toPlainString();
    }

}
