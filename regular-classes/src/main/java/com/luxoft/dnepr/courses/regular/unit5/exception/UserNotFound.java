package com.luxoft.dnepr.courses.regular.unit5.exception;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 11.05.13
 * Time: 14:31
 * To change this template use File | Settings | File Templates.
 */
public class UserNotFound extends Exception {
    public UserNotFound(String message) {
        super(message);
    }
}
