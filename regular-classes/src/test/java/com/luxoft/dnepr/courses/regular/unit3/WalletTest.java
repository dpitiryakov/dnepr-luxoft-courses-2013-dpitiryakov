package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.exceptions.InsufficientWalletAmountException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.LimitExceededException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.WalletIsBlockedException;
import org.junit.*;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 26.04.13
 * Time: 7:42
 * To change this template use File | Settings | File Templates.
 */
public class WalletTest {
    static Long id;
    static BigDecimal amount;
    static WalletStatus status;
    static BigDecimal maxAmount;
    static WalletInterface wallet;

    @BeforeClass
    public static  void setFields(){
        id=10L;
        amount=new BigDecimal(100);
        status=WalletStatus.ACTIVE;
        maxAmount=new BigDecimal(200);
        wallet=new Wallet(id,amount,status,maxAmount);
    }

    @Test
    public void testCheckWithdrawal(){
        wallet.setStatus(WalletStatus.BLOCKED);
        BigDecimal amountToWithdraw=new BigDecimal(20);
        try {
            wallet.checkWithdrawal(amountToWithdraw);
            fail("Wallet status should be blocked and WalletIsBlockedException should be thrown.");
        } catch (WalletIsBlockedException e) {
            System.out.println("testCheckWithdrawal caught WalletIsBlockedException. Good.");
        } catch (InsufficientWalletAmountException e) {
            fail("Wrong exception was caught.");
        }
        wallet.setStatus(WalletStatus.ACTIVE);
        amountToWithdraw=new BigDecimal(1000);
        try {
            wallet.checkWithdrawal(amountToWithdraw);
            fail("No exception was thrown. InsufficientWalletAmountException should be thrown.");
        } catch (WalletIsBlockedException e) {
            fail("InsufficientWalletAmountException should be thrown.");
        } catch (InsufficientWalletAmountException e) {
            System.out.println("testCheckWithdrawal caught InsufficientWalletAmountException. Good");
        }

        amountToWithdraw=new BigDecimal(10);
        try {
            wallet.checkWithdrawal(amountToWithdraw);
        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
            fail("No exception should be thrown.");
        } catch (InsufficientWalletAmountException e) {
            e.printStackTrace();
            fail("No exception should be thrown.");
        }
    }

    @Test
    public void testWithdraw(){
        wallet.withdraw(new BigDecimal(10));
        assertEquals(new BigDecimal(90),wallet.getAmount());
    }

    @Test
    public void testCheckTransfer(){
        BigDecimal amountToTransfer=new BigDecimal(1000);
        try {
            wallet.checkTransfer(amountToTransfer);
            fail("LimitExceededException should be thrown");
        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
            fail("LimitExceededException should be thrown instead of this");
        } catch (LimitExceededException e) {
            System.out.println("LimitExceededException was thrown. Good.");
        }

        amountToTransfer=new BigDecimal(10);
        wallet.setStatus(WalletStatus.BLOCKED);
        try {
            wallet.checkTransfer(amountToTransfer);
            fail("WalletIsBlockedException should be thrown");
        } catch (WalletIsBlockedException e) {
            System.out.println("WalletIsBlockedException  was thrown. Good.");
        } catch (LimitExceededException e) {
            e.printStackTrace();
            fail("WalletIsBlockedException should be thrown instead of this.");
        }

        wallet.setStatus(WalletStatus.ACTIVE);
        try {
            wallet.checkTransfer(amountToTransfer);
        } catch (WalletIsBlockedException e) {
            e.printStackTrace();
            fail("No exceptions should be thrown");
        } catch (LimitExceededException e) {
            e.printStackTrace();
            fail("No exceptions should be thrown");
        }
    }

    public void testTransfer(){
        wallet.transfer(new BigDecimal(10));
        assertEquals(new BigDecimal(100),wallet.getAmount());
    }

}
