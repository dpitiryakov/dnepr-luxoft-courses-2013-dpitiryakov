package com.luxoft.dnepr.courses.regular.unit2;

import org.junit.Test;

import java.util.GregorianCalendar;

import static org.junit.Assert.*;


public class BookTest {
    private ProductFactory productFactory = new ProductFactory();

    @Test
    public void testClone() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book secondBook=(Book)book.clone();
        assertEquals(book.getPublicationDate(),secondBook.getPublicationDate());
        book.setPublicationDate(new  GregorianCalendar(2001, 0, 1).getTime());
        //assertNotEquals(book.getPublicationDate(),secondBook.getPublicationDate());
    }

    @Test
    public void testEquals() throws Exception {
        Book book = productFactory.createBook("code", "Thinking in Java", 200, new GregorianCalendar(2006, 0, 1).getTime());
        Book secondBook=(Book)book.clone();
        assertEquals(true,book.equals(secondBook));
        book.setPrice(10);
        assertEquals(true,book.equals(secondBook));
        assertEquals(false,book.equals(null));
        assertEquals(true, book.equals(book));
        assertEquals(book.equals(secondBook),secondBook.equals(book));
        Book thirdBook=(Book)book.clone();
        thirdBook.setCode("br");
        assertEquals(false,book.equals(thirdBook));
        assertEquals(true,book.equals(secondBook));
    }
}
