package com.luxoft.dnepr.courses.regular.unit3;

import com.luxoft.dnepr.courses.regular.unit3.errors.IllegalJavaVersionError;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.NoUserFoundException;
import com.luxoft.dnepr.courses.regular.unit3.exceptions.TransactionException;
import junit.framework.Assert;
import junit.framework.TestCase;
import org.junit.BeforeClass;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.Assert.assertEquals;
import static junit.framework.TestCase.fail;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 26.04.13
 * Time: 7:53
 * To change this template use File | Settings | File Templates.
 */
public class BankTest {
    static Bank bank;
    static Wallet walet1,walet2,walet3;
    static User user1,user2,user3;


    @BeforeClass
    public static void init(){
        walet1=new Wallet(1L,new BigDecimal(100),WalletStatus.ACTIVE,new BigDecimal(149.5564));
        walet2=new Wallet(2L,new BigDecimal(100),WalletStatus.BLOCKED,new BigDecimal(200));
        walet3=new Wallet(3L,new BigDecimal(50),WalletStatus.ACTIVE,new BigDecimal(200));
        user1=new User(1L,"User1",walet1);
        user2=new User(2L,"User2",walet2);
        user3=new User(3L,"User3",walet3);
        bank=new Bank();
        Map<Long,UserInterface> users=new HashMap<>();
        users.put(user1.getId(),user1);
        users.put(user2.getId(),user2);
        users.put(user3.getId(),user3);
        bank.setUsers(users);
    }

    @Test
    public void testIllegalJavaVersionError(){
        System.out.println(System.getProperty("java.version"));
        try{
            Bank testBank =new Bank("1.5");
            fail("Should be thrown IllegalJavaVersionError");
        } catch (IllegalJavaVersionError e){
            System.out.println("IllegalJavaVersionError caught. Good.");
        }
        try{
            Bank testBank =new Bank("1.7.0_15");
        } catch (IllegalJavaVersionError e){
            fail("Shouldn't be any exception");
        }
    }



    @Test
    public void testMakeMoneyTransaction() {
        try {
            bank.makeMoneyTransaction(100L,10L,new BigDecimal(100));
            fail("Should be NoUserFoundException thrown");
        } catch (NoUserFoundException e) {
            System.out.println("NoUserFoundException was thrown. Good.");
        } catch (TransactionException e) {
            fail("Should be NoUserFoundException thrown instead of this.");
        }

        try {
            bank.makeMoneyTransaction(3L,1L,new BigDecimal(60));
            fail("Should be TransactionException thrown");
        } catch (NoUserFoundException e) {
            fail("Should be TransactionException thrown instead of this.");
        } catch (TransactionException e) {
            TestCase.assertEquals("User 'User3' has insufficient funds (50.00 < 60.00)",e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(1L,2L,new BigDecimal(60));
            fail("Should be TransactionException thrown");
        } catch (NoUserFoundException e) {
            fail("Should be TransactionException thrown instead of this.");
        } catch (TransactionException e) {
            TestCase.assertEquals("User 'User2' wallet is blocked",e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(3L,1L,new BigDecimal(50));
            fail("Should be TransactionException thrown");
        } catch (NoUserFoundException e) {
            fail("Should be TransactionException thrown instead of this.");
        } catch (TransactionException e) {
            TestCase.assertEquals("User 'User1' wallet limit exceeded (100.00 + 50.00 > 149.56)",e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(3L,1L,new BigDecimal(50));
            fail("Should be TransactionException thrown");
        } catch (NoUserFoundException e) {
            fail("Should be TransactionException thrown instead of this.");
        } catch (TransactionException e) {
            assertEquals("User 'User1' wallet limit exceeded (100.00 + 50.00 > 149.56)",e.getMessage());
        }

        try {
            bank.makeMoneyTransaction(3L,1L,new BigDecimal(40));
            assertEquals(bank.getUsers().get(1L).getWallet().getAmount(),new BigDecimal(140));
        } catch (NoUserFoundException e) {
            fail("Shouldn't be any exception");
        } catch (TransactionException e) {
            fail("Shouldn't be any exception");
        }


    }
}
