package com.luxoft.dnepr.courses.regular.unit4;

import org.junit.Before;
import org.junit.Test;
import org.junit.*;

import java.util.ArrayList;
import java.util.Set;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertArrayEquals;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 28.04.13
 * Time: 22:17
 * To change this template use File | Settings | File Templates.
 */
public class EqualSetTest {

    @Before
    public void init(){

    }

    @Test
    public void testSize() {
        Set<String> myset=new EqualSet<>();
        assertEquals(0,myset.size());
        myset.add("Hello");
        myset.add("Hello");
        assertEquals(1,myset.size());
        myset.clear();
        assertEquals(0,myset.size());
    }

    @Test
    public void testIsEmpty() {
        Set<String> myset=new EqualSet<>();
        assertEquals(true,myset.isEmpty());
        myset.add("Hello");
        myset.add("Hello");
        assertEquals(false,myset.isEmpty());
        myset.clear();
        assertEquals(true,myset.isEmpty());
    }

    @Test
    public void testContains()  {
        Set<Double> myset=new EqualSet<>();
        assertEquals(true,myset.isEmpty());
        myset.add(12d);
        myset.add(123d);
        myset.add(123d);
        assertEquals(false, myset.isEmpty());
        myset.clear();
        assertEquals(true,myset.isEmpty());
    }

    @Test
    public void testAdd()  {
        Set<Double> myset=new EqualSet<Double>();
        myset.add(12d);
        myset.add(123d);
        myset.add(123d);
        myset.add(12d);
        assertArrayEquals(new Object[]{12d, 123d},myset.toArray());

        myset.clear();
        assertEquals(true,myset.isEmpty());

        myset.add(null);
        Assert.assertEquals(1,myset.size());
        myset.add(null);
        Assert.assertEquals(1,myset.size());
        myset.add(2d);
        myset.add(null);
        Assert.assertEquals(2,myset.size());

    }



    @Test
    public void testRemove() {
        Set<Double> myset=new EqualSet<>();
        myset.add(12d);
        myset.add(123d);
        myset.remove(123d);
        Assert.assertEquals(1,myset.size());
    }

    @Test
    public void testContainsAll() {
        Set<Double> myset=new EqualSet<>();
        myset.add(1d);
        myset.add(2d);
        myset.add(3d);
        Assert.assertEquals(true,myset.containsAll(new ArrayList<Double>() {{
            add(1d);
            add(2d);
        }}));
        Assert.assertEquals(false,myset.containsAll(new ArrayList<Double>() {{
            add(1d);
            add(2d);
            add(3d);
            add(4d);
        }}));

    }

    @Test
    public void testAddAll() {
        Set<Double> myset=new EqualSet<>();
        myset.add(1d);
        myset.add(2d);
        myset.add(3d);
        Assert.assertEquals(true,myset.addAll(new ArrayList<Double>() {{
            add(3d);
            add(4d);
            add(null);
            add(null);
        }}));
        Assert.assertEquals(5,myset.size());

    }

    @Test
    public void testRetainAll() {
        Set<Double> myset=new EqualSet<>();
        myset.add(1d);
        myset.add(2d);
        myset.add(3d);
        myset.add(null);

        Set<Double> tset=new EqualSet<>();
        tset.add(2d);
        tset.add(3d);
        tset.add(null);

        Assert.assertEquals(true,myset.retainAll(tset));
        Assert.assertEquals(3,myset.size());

    }



}
