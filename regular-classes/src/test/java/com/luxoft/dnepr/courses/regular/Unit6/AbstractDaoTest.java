package com.luxoft.dnepr.courses.regular.Unit6;

import com.luxoft.dnepr.courses.regular.unit5.dao.IDao;
import com.luxoft.dnepr.courses.regular.unit5.dao.RedisDaoImpl;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserAlreadyExist;
import com.luxoft.dnepr.courses.regular.unit5.exception.UserNotFound;
import com.luxoft.dnepr.courses.regular.unit5.model.Redis;
import org.junit.Assert;
import org.junit.Test;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 11.05.13
 * Time: 16:28
 * To change this template use File | Settings | File Templates.
 */
public class AbstractDaoTest {

    @Test
    public void daoTest(){
        IDao<Redis> redisDao = new RedisDaoImpl();
        Redis redis1 = new Redis(new Long(1), 1);
        Redis redis2 = new Redis(new Long(1), 2);
        Redis redis3 = new Redis(new Long(2), 2);


        try {
            redisDao.save(redis1);
            redisDao.save(redis2);
            Assert.fail("Must be exception UserAlreadyExist");
        } catch (UserAlreadyExist e) {}
        try {
            redisDao.update(redis3);
            Assert.fail("Must be exception UserТщеАщгтв");
        } catch (UserNotFound e) {}

        Assert.assertEquals(null, redisDao.get(0));

        Assert.assertEquals(redis1, redisDao.get(1));

        Assert.assertEquals(false, redisDao.delete(20));
        Assert.assertEquals(true, redisDao.delete(1));
        Assert.assertEquals(null, redisDao.get(1));



    }


}
