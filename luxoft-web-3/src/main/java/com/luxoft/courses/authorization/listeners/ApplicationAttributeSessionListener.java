package com.luxoft.courses.authorization.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationAttributeSessionListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent httpSessionBindingEvent) {
        HttpSession session= httpSessionBindingEvent.getSession();

        if (session.getAttribute("role").equals("admin")){
            incrementSessionAttribute(session,ListnerConstans.ACTIVE_SESSIONS_ADMIN);
        } else if (session.getAttribute("role").equals("user")){
            incrementSessionAttribute(session,ListnerConstans.ACTIVE_SESSIONS_USER);
        }

    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent httpSessionBindingEvent) {

    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {

    }

    private void incrementSessionAttribute(HttpSession session, String Attribute) {
        ((AtomicLong) session.getServletContext().getAttribute(Attribute)).getAndIncrement();
    }
}
