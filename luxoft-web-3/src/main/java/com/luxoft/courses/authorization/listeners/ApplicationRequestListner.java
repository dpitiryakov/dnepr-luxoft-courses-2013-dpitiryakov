package com.luxoft.courses.authorization.listeners;

import javax.servlet.ServletContext;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.atomic.AtomicLong;


public class ApplicationRequestListner implements ServletRequestListener {
    @Override
    public void requestDestroyed(ServletRequestEvent servletRequestEvent) {

    }

    @Override
    public void requestInitialized(ServletRequestEvent servletRequestEvent) {
        ServletContext servletContext = servletRequestEvent.getServletContext();
        HttpServletRequest srvltReq = (HttpServletRequest) servletRequestEvent.getServletRequest();
        String method = srvltReq.getMethod();

        ((AtomicLong) servletContext.getAttribute(
                ListnerConstans.HTTP_REQUESTS_COUNTER_TOTAL)).getAndIncrement();

        if (method.equals("POST")) {
            ((AtomicLong) servletContext.getAttribute(
                    ListnerConstans.HTTP_REQUESTS_COUNTER_POST)).getAndIncrement();
        } else if (method.equals("GET")) {
            ((AtomicLong) servletContext.getAttribute(
                    ListnerConstans.HTTP_REQUESTS_COUNTER_GET)).getAndIncrement();
        } else {
            ((AtomicLong) servletContext.getAttribute(
                    ListnerConstans.HTTP_REQUESTS_COUNTER_OTHER)).getAndIncrement();
        }

    }
}
