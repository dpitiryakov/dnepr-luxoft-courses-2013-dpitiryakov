package com.luxoft.courses.authorization;

import com.luxoft.courses.authorization.listeners.ListnerConstans;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created with IntelliJ IDEA.
 * User: Denis
 * Date: 17.06.13
 * Time: 10:14
 * To change this template use File | Settings | File Templates.
 */
public class AdminPage extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();

        String activeSessions= getAttribute(ListnerConstans.ACTIVE_SESSIONS);
        String activeSessionsUser= getAttribute(ListnerConstans.ACTIVE_SESSIONS_USER);
        String activeSessionsAdmin= getAttribute(ListnerConstans.ACTIVE_SESSIONS_ADMIN);
        String requestsTotal= getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_TOTAL);
        String requestsPost= getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_POST);
        String requestsGet= getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_GET);
        String requestsOther= getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_OTHER);


        writer.print("<html>\n" +
                "\t<head>\n" +
                "\t\t<title> A Web Page</title>\n" +
                "\t</head>\n" +
                "\t<body>\n" +
                "\t\t<div id=\"container\" align=\"center\" style=\"width: 40%; margin: 0 auto\"> \n" +
                "\t\t\t<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\">\n" +
                "   \t\t\t\t<tr align=\"left\" bgcolor=\"#999999\">\n" +
                "   \t\t\t\t\t<th > Parameter </th>\t<th> Value </th>\n" +
                "   \t\t\t\t</tr>\n" +
                "   \t\t\t\t<tr bgcolor=\"#D0C5C5\">\n" +
                "    \t\t\t\t<td > Active Sessions </td> <td> "+activeSessions+" </td>\n" +
                "    \t\t\t</tr>\n" +
                "    \t\t\t<tr bgcolor=\"#E4E4E4\">\n" +
                "    \t\t\t\t<td > Active Sessions (ROLE user) </td> <td> "+activeSessionsUser+" </td>\n" +
                "    \t\t\t</tr>\n" +
                "    \t\t\t<tr bgcolor=\"#D0C5C5\">\n" +
                "    \t\t\t\t<td > Active Sessions (ROLE admin)</td> <td> "+activeSessionsAdmin+" </td>\n" +
                "    \t\t\t</tr>\n" +
                "    \t\t\t<tr bgcolor=\"#E4E4E4\">\n" +
                "    \t\t\t\t<td > Total Count of HttpRequest </td> <td> "+requestsTotal+" </td>\n" +
                "    \t\t\t</tr>\n" +
                "    \t\t\t<tr bgcolor=\"#D0C5C5\">\n" +
                "    \t\t\t\t<td > Total Count of POST HttpRequests </td> <td> "+requestsPost+" </td>\n" +
                "    \t\t\t</tr>\n" +
                "    \t\t\t<tr bgcolor=\"#E4E4E4\">\n" +
                "    \t\t\t\t<td > Total Count of GET HttpRequests </td> <td> "+requestsGet+" </td>\n" +
                "    \t\t\t</tr>\n" +
                "    \t\t\t<tr bgcolor=\"#D0C5C5\">\n" +
                "    \t\t\t\t<td > Total Count of Other HttpRequests </td> <td> "+requestsOther+" </td>\n" +
                "   \t\t\t\t</tr>\n" +
                "\t\t</div>\n" +
                "\t</body>\n" +
                "</html>");
    }

    private String getAttribute(String attributeName) {
        return getServletContext().getAttribute(attributeName).toString();
    }
}
