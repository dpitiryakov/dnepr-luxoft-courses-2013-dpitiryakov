package com.luxoft.courses.authorization.listeners;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.concurrent.atomic.AtomicLong;


public class ApplicationSessionListner implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {
        HttpSession session=httpSessionEvent.getSession();
        incrementSessionAttribute(session, ListnerConstans.ACTIVE_SESSIONS);
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent httpSessionEvent) {
        HttpSession session=httpSessionEvent.getSession();

        decrementSessionAttribute(session, ListnerConstans.ACTIVE_SESSIONS);
        if (session.getAttribute("role").equals("admin")){
            decrementSessionAttribute(session, ListnerConstans.ACTIVE_SESSIONS_ADMIN);
        } else if (session.getAttribute("role").equals("user")){
            decrementSessionAttribute(session, ListnerConstans.ACTIVE_SESSIONS_USER);
        }
    }


    private void incrementSessionAttribute(HttpSession session, String Attribute) {
        ((AtomicLong) session.getServletContext().getAttribute(Attribute)).getAndIncrement();
    }

    private void decrementSessionAttribute(HttpSession session, String Attribute) {
        ((AtomicLong) session.getServletContext().getAttribute(Attribute)).getAndDecrement();
    }
}
