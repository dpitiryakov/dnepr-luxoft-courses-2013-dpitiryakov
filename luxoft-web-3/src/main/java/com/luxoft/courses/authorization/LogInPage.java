package com.luxoft.courses.authorization;

import com.google.gson.Gson;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;


public class LogInPage extends HttpServlet {
    public static final String WRONG_LOGIN_OR_PASSWORD_OR_USER_TYPE = "Wrong login or password or user type";

    private boolean errorFlag=false; // false - no mistake, true - there is a mistake
    private static List<User> users;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String contextParamJSON=getServletContext().getInitParameter("users");
        users = feelUsers(contextParamJSON);
    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        String tmpError="";
        if (errorFlag){
            tmpError=WRONG_LOGIN_OR_PASSWORD_OR_USER_TYPE;
        }
        writer.print("<html>\n" +
                "\t<head>\n" +
                "\t\t<title> A Web Page</title>\n" +
                "\t</head>\n" +
                "\n" +
                "\t<body>\n" +
                "\t\t<div id=\"container\" align=\"center\"> \n" +
                "\t\t\t<form action=\"index.html\" method=\"post\">\n" +
                "\t\t\t\t<p>\n" +
                "\t\t\t\t\t<br>\n" +
                "\t\t\t\t\t\t Login:\n" +
                "\t\t\t\t\t\t<input name=\"name\" id=\"nameField\" type=\"text\"  />\n" +
                "\t\t\t\t\t\t" + tmpError + "\n" +
                "\t\t\t\t\t</br>\n" +
                "\t\t\t\t\t<br> \n" +
                "\t\t\t\t\t\tPassword:\n" +
                "\t\t\t\t\t\t<input name=\"password\" id=\"passwordField\" type=\"password\"  />\n" +
                "\t\t\t\t\t</br>\n" +
                "\t\t\t\t\t<br> \n" +
                "\t\t\t\t\t\tRole: <br>\n" +
                "\t\t\t\t\t\t<input name=\"role\" type=\"radio\" value=\"user\"/>user <br>\n" +
                "\t\t\t\t\t\t<input name=\"role\" type=\"radio\" value=\"admin\"/>admin <br>\n" +
                "\t\t\t\t\t</br>\n" +
                "\t\t\t\t\t<input name=\"loginBtn\" type=\"submit\" value=\"Sign In\">\n" +
                "\t\t\t\t</p>\n" +
                "\t\t\t</form>\n" +
                "\n" +
                "\t\t</div>\n" +
                "\n" +
                "\t</body>\n" +
                "\n" +
                "</html>");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException,IOException {
        String nameTyped=request.getParameter("name");
        String passwordTyped=request.getParameter("password");
        String roleTyped=request.getParameter("role");
        User userTyped=new User(nameTyped,passwordTyped,roleTyped);



        if (users.contains(userTyped)){
            errorFlag=false;
            HttpSession session=request.getSession();
            session.setAttribute("login", nameTyped);
            session.setAttribute("role", roleTyped);
            response.sendRedirect("/user");
        } else {
            errorFlag=true;
            response.sendRedirect("/index.html");
        }
    }

    private List<User> feelUsers(String contextParamJSON) {
        contextParamJSON="["+contextParamJSON.substring(1,contextParamJSON.length()-1)+"]";
        Gson gson = new Gson();
        User[] myTypes = gson.fromJson(contextParamJSON, User[].class);
        return Arrays.asList(myTypes);
    }
}
