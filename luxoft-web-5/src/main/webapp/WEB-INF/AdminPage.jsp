
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@page session="false" %>
<%@ page import="com.luxoft.courses.authorization.listeners.ListnerConstans" %>
<html>
<head>
    <title> A Web Page</title>
</head>
<body>
<div id="container" align="center" style="width: 40%; margin: 0 auto">
    <table width="100%" border="0" cellspacing="0" cellpadding="4">
        <tr align="left" bgcolor="#999999">
            <th > Parameter </th>	<th> Value </th>
        </tr>
        <tr bgcolor="#D0C5C5">
            <td > Active Sessions </td> <td> <%=application.getAttribute(ListnerConstans.ACTIVE_SESSIONS)%> </td>
        </tr>
        <tr bgcolor="#E4E4E4">
            <td > Active Sessions (ROLE user) </td> <td> <%=application.getAttribute(ListnerConstans.ACTIVE_SESSIONS_USER)%> </td>
        </tr>
        <tr bgcolor="#D0C5C5">
            <td > Active Sessions (ROLE admin)</td> <td> <%=application.getAttribute(ListnerConstans.ACTIVE_SESSIONS_ADMIN)%> </td>
        </tr>
        <tr bgcolor="#E4E4E4">
            <td > Total Count of HttpRequest </td> <td>  <%=application.getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_TOTAL)%> </td>
        </tr>
        <tr bgcolor="#D0C5C5">
            <td > Total Count of POST HttpRequests </td> <td>  <%=application.getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_POST)%> </td>
        </tr>
        <tr bgcolor="#E4E4E4">
            <td > Total Count of GET HttpRequests </td> <td>  <%=application.getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_GET)%> </td>
        </tr>
        <tr bgcolor="#D0C5C5">
            <td > Total Count of Other HttpRequests </td> <td>  <%=application.getAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_OTHER)%> </td>
        </tr>
        </table>
</div>
</body>
</html>