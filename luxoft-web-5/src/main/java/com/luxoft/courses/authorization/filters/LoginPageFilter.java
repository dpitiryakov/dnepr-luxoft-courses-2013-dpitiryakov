package com.luxoft.courses.authorization.filters;


import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class LoginPageFilter implements Filter {
    private FilterConfig filterConfig;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.filterConfig = filterConfig;
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        if (filterConfig.getServletContext().getAttribute("error") != null &&
                request.getParameter("error") == null) {
            response.sendRedirect(filterConfig.getServletContext().getContextPath() + "index.html" + "?error=wrong");
        }
        filterChain.doFilter(request, response);
    }

    @Override
    public void destroy() {
    }
}
