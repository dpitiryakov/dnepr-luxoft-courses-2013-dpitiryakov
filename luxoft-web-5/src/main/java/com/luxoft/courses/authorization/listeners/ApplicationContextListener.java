package com.luxoft.courses.authorization.listeners;


import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.util.concurrent.atomic.AtomicLong;

public class ApplicationContextListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        servletContext.setAttribute(ListnerConstans.ACTIVE_SESSIONS, new AtomicLong(0));
        servletContext.setAttribute(ListnerConstans.ACTIVE_SESSIONS_ADMIN, new AtomicLong(0));
        servletContext.setAttribute(ListnerConstans.ACTIVE_SESSIONS_USER, new AtomicLong(0));
        servletContext.setAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_GET, new AtomicLong(0));
        servletContext.setAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_OTHER, new AtomicLong(0));
        servletContext.setAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_POST, new AtomicLong(0));
        servletContext.setAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_TOTAL, new AtomicLong(0));

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ServletContext servletContext = sce.getServletContext();
        servletContext.removeAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_TOTAL);
        servletContext.removeAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_POST);
        servletContext.removeAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_OTHER);
        servletContext.removeAttribute(ListnerConstans.HTTP_REQUESTS_COUNTER_GET);
        servletContext.removeAttribute(ListnerConstans.ACTIVE_SESSIONS_USER);
        servletContext.removeAttribute(ListnerConstans.ACTIVE_SESSIONS_ADMIN);
        servletContext.removeAttribute(ListnerConstans.ACTIVE_SESSIONS);
    }
}