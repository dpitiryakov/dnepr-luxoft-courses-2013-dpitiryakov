package com.luxoft.courses.authorization;

import com.google.gson.Gson;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;


public class LogInPage extends HttpServlet {
    public static final String WRONG_LOGIN_OR_PASSWORD_OR_USER_TYPE = "Wrong login or password or user type";

    private static List<User> users;

    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String contextParamJSON = getServletContext().getInitParameter("users");
        users = feelUsers(contextParamJSON);
    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String nameTyped = request.getParameter("name");
        String passwordTyped = request.getParameter("password");
        User userTyped = new User(nameTyped, passwordTyped);


        if (users.contains(userTyped)) {
            HttpSession session = request.getSession();
            session.setAttribute("login", nameTyped);
            session.setAttribute("role", userTyped.getRole());
            response.sendRedirect("/user");
        } else {
            getServletConfig().getServletContext().setAttribute("error", "error");
            response.sendRedirect("/index.html");
        }
    }

    private List<User> feelUsers(String contextParamJSON) {
        contextParamJSON = "[" + contextParamJSON.substring(1, contextParamJSON.length() - 1) + "]";
        Gson gson = new Gson();
        User[] myTypes = gson.fromJson(contextParamJSON, User[].class);
        return Arrays.asList(myTypes);
    }
}
