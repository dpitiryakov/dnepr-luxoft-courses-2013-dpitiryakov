package com.luxoft.courses.authorization.listeners;

public interface ListnerConstans {
    String ACTIVE_SESSIONS = "Active Sessions";
    String ACTIVE_SESSIONS_USER = "Active Sessions User";
    String ACTIVE_SESSIONS_ADMIN = "Active Sessions Admin";
    String HTTP_REQUESTS_COUNTER_TOTAL = "Total Http requests";
    String HTTP_REQUESTS_COUNTER_GET = "Get Http requests";
    String HTTP_REQUESTS_COUNTER_POST = "Post Http requests";
    String HTTP_REQUESTS_COUNTER_OTHER = "Other Http requests";
}
